# Bienvenue

Les Ateliers du Web sémantique ont lieu en visio les premiers mardi du mois entre 10h et 11h depuis janvier 2021.
Ils ont été lancés suite à l'action nationale de formation 
[« Initiation au SPARQL et à la diffusion de données en RDF »](https://rbdd.cnrs.fr/spip.php?article337) organisée par 
le réseau Bases de données du CNRS du 7 au 8 décembre 2020. Cette action de formation a été suivie par 
[« SPARQL Avancé »](https://rbdd.cnrs.fr/spip.php?article355) en octobre 2021. Les Ateliers ont pour objectif de prolonger la formation et les échanges entre les anciens 
stagiaires de ces actions de formation.

Ces pages visent à partager avec un plus large public le contenu de ces ateliers.