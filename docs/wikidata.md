# Dépôt de données sur Wikidata avec OpenRefine

Notes de l'atelier du mardi 1er février 2022 (10h-11h30) en visio.

Auteur : Jean-Baptiste Pressac (Centre de recherche bretonne et celtique - CNRS/UBO)

Publié le 25 février 2022 - Mis à jour le 14 avril 2022

## Objectifs de l'atelier

L'objectif de l'atelier est d'expérimenter le dépôt de données au format CSV sur la base de connaissance collaborative Wikidata 
à l'aide du logiciel de traitement de données *OpenRefine*.

Les données choisies sont les notices UNIMARC de publications en breton pour l'année 2021 disponibles sur le 
catalogue général de la BnF (Bibliothèque nationale de France).

Les étapes du dépôt sont les suivantes :

1. Export des notices des ouvrages en breton édités en 2021 à partir du catalogue général de la BnF,
2. Création d'un nouveau projet dans le logiciel de traitement de données OpenRefine,
3. Import des notices dans ce nouveau projet,
4. Traitement des données répétables (un document peut avoir plusieurs auteurs)
5. Réconciliation des livres, des producteurs (auteurs, illustrateurs, traducteurs, etc.) et des maisons d’éditions avec des entités Wikidata, 
6. Création des éléments manquants dans Wikidata et des relations entre eux (par ex. créer le livre _An div ran a oa bras o beg_ 
et son auteur, _Pierre Delye_ et déclarer ce dernier comme auteur du livre).

La *réconciliation* consiste à faire correspondre un jeu de données avec une source externe. Dans notre cas, OpenRefine
soumettra à Wikidata les titres des livres, les noms des auteurs et Wikidata renverra 
à OpenRefine des données Wikidata (appelées _éléments_ ou _items_) dont le label correspond plus ou moins aux titres et 
aux noms des auteurs.

L'étape préalable de la réconciliation évite la création de doublons lors du dépôt de données sur Wikidata.

### A propos des licences

La BnF diffuse ses données selon la « [Licence Ouverte](https://data.bnf.fr/fr/docs/Licence-Ouverte-Open-Licence.pdf) », 
qui a pour condition de réutilisation le maintien de la mention de la source et la date de récupération des données. 

!!! cite "Source : [Conditions de réutilisations des données de la BnF](https://www.bnf.fr/fr/conditions-de-reutilisations-des-donnees-de-la-bnf)"

    La BnF a, depuis le 1er janvier 2014, placé ses métadonnées descriptives (données bibliographiques et d'autorité) 
    sous la « [Licence Ouverte / Open Licence](https://data.bnf.fr/fr/docs/Licence-Ouverte-Open-Licence.pdf) » 
    de l'État. L’utilisation de ces métadonnées est libre et gratuite sous réserve du maintien 
    de la mention de leur source et de l’indication de leur date de récupération.


Par ailleurs, toute contribution à Wikidata est placé sous la licence [CC0](https://fr.wikipedia.org/wiki/Licence_CC0). 
Licence qui permet la réutilisation des données sans citation avec renoncement des droits. Wikidata incite toutefois à citer ses sources
lors de la création de nouvelles données, ce que nous ne manquerons pas de faire.

!!! Info

    Il est possible de vérifier si les données sont éligibles au dépôt sur Wikidata depuis [Wikidata:Notoriété](https://www.wikidata.org/wiki/Wikidata:Notability/fr).

## Installation de OpenRefine

La dernière version de OpenRefine peut être téléchargée depuis [https://github.com/OpenRefine/OpenRefine/releases](https://github.com/OpenRefine/OpenRefine/releases).

!!! info

    OpenRefine nécessite l’installation préalable de Java. Sous Windows, afin d’éviter toute incompatibilité avec les 
    éventuelles versions de Java installées sur votre ordinateur, l’installation du bundle OpenRefine + Java pour 
    Windows est conseillée (le fichier openrefine-win-with-java-x.y.z.zip).

    Pour des informations détaillées sur l'installation et la configuration de OpenRefine, consultez le chapitre 
    [Installing OpenRefine](https://docs.openrefine.org/manual/installing) de la documentation 
    officielle. 

## Export CSV des notices UNIMARC des ouvrages en breton publiés en 2021 depuis le catalogue général de la BnF

Les notices des documents reçus au dépôt légal sont consultables sur le catalogue général de la BnF. Elles sont 
disponibles au format public (voir la notice de [_An div ran a oa bras o beg_](https://catalogue.bnf.fr/ark:/12148/cb46811801p.public), 
traduction en breton par Mark Kerrain de _Les deux grenouilles à grande bouche_ de Pierre Delye, 
illustrations de Cécile Hudrisier, publié en 2021 par Sav-Heol) et dans une version UNIMARC plus détaillée 
(c.f. la version UNIMARC de la notice de [_An div ran a oa bras o beg_](https://catalogue.bnf.fr/ark:/12148/cb46811801p.unimarc)). 

Le catalogue concerne des documents de toute nature : livres, périodiques, documents électroniques, CD audios, 
CD-ROM, etc. Nous ne l'avons pas fait pour cet atelier, mais vous pouvez limiter l'export aux textes imprimés.

Depuis le [moteur de recherche avancé du catalogue général](https://catalogue.bnf.fr/recherche-avancee.do?pageRech=rav), 
on choisit les critères suivants : 

- Par nature de document > Type de document = Texte imprimé et livre numérique
- Par langue > Langue du document = breton
- Par date de publication = 2021

![Sélection dans le formulaire de recherche avancé du catalogue général de la BnF des documents en breton publiés en 2021](img/2022-01-31 09_54_30-Window.jpg)

On télécharge ensuite les notices en UNIMARC avec _Tous les résultats > Exporter dans un tableau_.

![Export dans un fichier CSV de notices du catalogue général de la BnF](img/2022-01-31 09_55_16-Window.jpg)

Puis _Paramètrage professionnel_ en précisant les champs UNIMARC : `000;010$a;101;200;205;214;454;700;702;` 
(ne pas oublier le point-virgule final) et en cochant les cases "_1 colonne par sous-zone_" et "_Données avec étiquette_", 
faute de quoi le contenu des champs répétés seront concaténés.

La signification des champs UNIMARC est explicité dans le chapitre suivant.

![Paramètrage professionnel de l'export dans un fichier CSV de notices du catalogue général de la BnF](img/2022-03-02 09_24_30-Window.jpg)

Les exports dans des tableaux CSV peuvent durer plusieurs minutes et se solder par un message d'erreur.

!!! note "Note à propos de l'export des données avec étiquette"

    Si dans le paramètrage profesionnel de l'export dans un tableau d'une notice, _Données avec étiquette_
    n'est pas coché, les zones et sous-zone répétées seront contaténées sans caractère séparateur. Il ne sera donc 
    pas possible de distinguer les valeurs des zones et sous-zones répétées. 

    Par exemple, le livre [An urzh-paeañ](https://catalogue.bnf.fr/ark:/12148/cb46707672q.unimarc) est publié 
    par Hor yezh (Lannion) et imprimé par Ouestellio (Brest) comme le précisent les deux zones UNIMARC _214 – 
    Mention de publication, production, distribution/diffusion, fabrication_ :
    
    ```
    214 .0 $a [Lannion] $c Mouladurioù Hor yezh $d DL 2021
    214 .3 $a 29-Brest $c Moul. Ouestelio
    ```

    Avec un export en CSV sans étiquettes, la valeur de la colonne `214$a` aura pour valeur `[Lannion]29-Brest` et 
    la colonne `214$c` aura pour valeur `Mouladurioù Hor yezhMoul. Ouestelio`.

    Par contre, avec un export avec étiquettes, la valeur de la colonne 214$a aura pour valeur 
    `214$a[Lannion]214$a29-Brest` (notez la répétion de `214$a`) et la colonne `214$c` aura pour valeur 
    `214$cMouladurioù Hor yezh214$cMoul. Ouestelio`.

## Décryptage des champs UNIMARC choisis pour l'export

Le format de catalogage UNIMARC défini des champs de notices bibliographiques et de 
notices d'autorité pour les bibliothèques. En France, la référence pour l'UNIMARC est le site *Transition bibliographique* 
qui héberge les documents de référence sur ce format. La rubrique qui nous concerne est [Manuel UNIMARC : format bibliographique](https://www.transition-bibliographique.fr/unimarc/manuel-unimarc-format-bibliographique/).

Cependant, c'est l'étude de la version UNIMARC de la notice du livre en breton *[An div ran a oa bras o beg](https://catalogue.bnf.fr/ark:/12148/cb46811801p.unimarc)* 
qui a orienté nos choix des zones UNIMARC à exporter :

| Code du bloc ou de la zone | Nom du bloc ou de la zone             | Notes et exemples de valeurs                                                                                                                                                                        |
|:---------------------------|:--------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 000                        | *Bloc de l'identification*            | Ce champ apparait dans la colonne *Identifiant ARK;N° notice BnF;Type de notice ;Type de document* du fichier CSV exporté. Sa valeur diffère de celle affichée par la version UNIMARC de la notice. |
| 010$a                      | *Numéro (ISBN)*                       | ex. 20210610d2021                                                                                                                                                                                   |
| 101                        | *Langue de la ressource*             | ex. 101 1. $a bre $c fre                                                                                                                                                                            |
| 200                        | *Titre et mention de responsabilité* | ex.  200 1. $a An div ran a oa bras o beg $b Texte imprimé $f un istor kontet gant Pierre Delye $g skeudennet gant Cécile Hudrisier $g ha troet e brezhoneg gant Mark Kerrain                       |
| 205                        | *Mention d’édition*   ||
| 214                        | *Mention de publication, production, distribution/diffusion, fabrication*  | ex. 214 .0 $a Reuz [i.e. Le Rheu] $c Sav-Heol $d DL 2021                                                                                                                                            |
| 454                        | *Est une traduction de*  | ex. 454 .1 $t Les deux grenouilles à grande bouche                                                                                                                                                  |
| 700                        | *Nom de personne – Responsabilité principale*  | ex. 700 . $3 14492400 $o ISNI0000000363699480 $a Delye $b Pierre $f 1968-.... $4 070                                                                                                                |
| 702                        | *Nom de personne – Responsabilité secondaire*  | ex. 702 . $3 13583052 $o ISNI0000000121312685 $a Hudrisier $b Cécile $f 1976-.... $4 440                                                                                                            |


### Langue de la ressource (Zone 101)

Les zones peuvent être divisés en sous-zones et précédées par des indicateurs. Prenons le cas de la zone 101 
(Langue de la ressource) de *An div ran a oa bras o beg* :

`101 1. $a bre $c fre`

Le manuel de la [zone 101](https://www.transition-bibliographique.fr/wp-content/uploads/2018/07/B101-6-2010.pdf) sur le site de la Transisition bibliographique permet de déduire les informations suivantes :

- *An div ran a oa bras o beg* est la traduction d'une oeuvre originale ou d'une oeuvre intermédiaire. Le numéro de la zone est suivi par `1.` qui indique les valeurs de l'*indicateur 1* (`1`) et de l'indicateur 2 (`.` = indéfini). **L'indicateur 1 précise si la ressource est ou non une traduction** : La valeur `1` signifie que *la ressource est une traduction de l’œuvre originale ou d’une œuvre intermédiaire*. La valeur `2` signifie que *La ressource contient des traductions autres que des résumés traduits*. La nuance entre ces deux valeurs ne nous étant pas familière, nous les prendrons toutes les deux en compte.
- la langue du texte de *An div ran a oa bras o beg* est le français : La sous-zone `101$a`, qui sert à préciser la langue du texte, a pour valeur le code ISO 639-2 `bre`.
- la langue du texte original est le français : La sous-zone `101$c` qui sert à préciser la langue du texte original, a pour valeur le code ISO 639-2 `fre`.

Notez que les sous-zones sont répétables et qu'il est possible de préciser les langues intermédiaires. Le manuel de la [zone 101](https://www.transition-bibliographique.fr/wp-content/uploads/2018/07/B101-6-2010.pdf) donne notamment l'exemple suivant (dans le manuel, les indicateurs indéfinis sont signalés par un `#` au lieu d'un `.`. Pour une meilleure lisibilité, nous avons séparés dans l'exemple ci-dessous les sous-zones de leurs valeurs) :

`101 1# $a eng $b ger $b fre $c akk`

*Le texte d’origine en akkadien, langue ancienne, a d’abord été traduit pour partie en allemand et
pour partie en français. Une version anglaise a ensuite été établie à partir des traductions
allemande et française.*

**Dans le cadre de ce tutoriel, on se contentera d'exploiter la première sous-zone `101$a` sans se préocupper 
des autres sous-zones et des indicateurs.**

### Titre et mention de responsabilité (zone 200)

Le titre de *An div ran a oa bras o beg* est indiqué dans la sous-zone `$a` de la zone 200 (Titre et mention de responsabilité).

### Responsabilités (auteur, traducteur, illustrateur...) (bloc 7XX)

L'auteur (Pierre Delye) et l'illustratrice (Cécile Hudrisier) sont indiqués tels qu'ils apparaissent sur la couverture du livre dans les sous-zones `200 $f` = *un istor kontet gant Pierre Delye* (une histoire racontée par Pierre Delye) et `200 $g` = *skeudennet gant Cécile Hudrisier* (illustré par Cécile Hudrisier).

Pour une mention plus complète et sans ambiguité des personnes et des collectivités intervenants dans la création du document, on se reportera plutôt au bloc des responsabilités, qui regroupe les zones 700 à 742.

Par exemple, la zone 700 (Nom de personne – Responsabilité principale), permet de préciser l'identifiant de la notice de l'auteur, *Pierre Delye*, dans le catalogue général de la BnF ([14492400](https://catalogue.bnf.fr/ark:/12148/cb14492400v)) et sur ISNI (International Standard Name Identifier), un standart ISO d'identification de personnes et de collectivités impliqués dans les activités créatives ([ISNI0000000363699480](http://isni.org/isni/0000000363699480)) :

`700 .| $3 14492400 $o ISNI0000000363699480 $a Delye $b Pierre $f 1968-.... $4 070`

La sous-zone `700$4` précise le code de fonction. Le code de fonction de Pierre Delye a pour valeur 070, ce qui correspond à *Auteur* (pour une liste détaillées des fonctions, se reporter à l'[Annexe C – Codes de fonction – Liste numérique](https://www.transition-bibliographique.fr/wp-content/uploads/2021/06/BAnnC-2021.pdf) de la Transition bibliographique).

Le traducteur, Mark Kerrain, est indiqué dans la zone 702 (Nom de personne – Responsabilité secondaire) avec le code 730 (Traducteur) : 

`702 .| $3 11909642 $o ISNI000000012125805X $a Kerrain $b Mark $f 1952-.... $4 730`

**Les zones 700 à 702 ne concernent que les personnes. Les mentions de responsabilités des collectifs font l'objet des zones 
710 à 712 mais nous n'en tenons pas compte pour ce tutoriel.**

## Effectuer un premier versement à la main

Il vaut mieux tenter d’abord de créer *à la main* son premier livre sur Wikidata afin d’expérimenter 
les propriétés et les éventuelles suggestions et messages d’erreurs de Wikidata. Par exemple, le livre *[Ar roue Marc'h (Q110713431)](https://www.wikidata.org/wiki/Q110713431)* 
publié en 2019 a été créé à la main sur Wikidata à partir de [sa notice BnF](https://catalogue.bnf.fr/ark:/12148/cb468188330.public). 

Le premier versement est également l'occasion de réfléchir à la nature de la donnée à verser. Sur Wikidata, chaque *élément* (*item*) 
a une *nature* déclarée par [la propriété P31](https://www.wikidata.org/wiki/Property:P31), *instance of* ou *nature de l'élément*. Autrement
dit, chaque élément est l'instance d'une classe.

Les discussions au sujet de la modélisation des livres sur Wikidata a été consignée sur [Wikidata:WikiProject Books](https://www.wikidata.org/wiki/Wikidata:WikiProject_Books#Bibliographic_properties).
Les discutants ont choisi de se conformer partiellement au modèle FRBR de l'IFLA. Les éditions (les *manifestations*) sont 
représentées par la classe [version, edition, or translation](https://www.wikidata.org/wiki/Q3331189). Les traductions, 
modélisées en FRBR au niveau de l'expression, sont intégrées à cette classe dont [une des propriétés](https://www.wikidata.org/wiki/Wikidata:WikiProject_Books#Edition_item_properties) est l'ISBN.

Les auteurs (associés à l'œuvre en FRBR) et la langue (associé à l'expression en FRBR) peuvent être associés directement 
à l'édition dans le modèle de Wikidata.

Wikidata incite à citer ses sources. Nous avons choisi de créer une [référence](https://www.wikidata.org/wiki/Help:Sources/fr) par 
*statement* (ou *[déclaration](https://www.wikidata.org/wiki/Help:Statements/fr)*) avec [affirmé dans (P248)](https://www.wikidata.org/wiki/Property:P248) = [catalogue général de la BnF (Q15222191)](https://www.wikidata.org/wiki/Q15222191) 
et [URL de la référence (P854)](https://www.wikidata.org/wiki/Property:P854) = l’identifiant pérenne de la notice, par ex. http://ark.bnf.fr/ark:/12148/cb468188330.public 
et [date de consultation (P813)](https://www.wikidata.org/wiki/Property:P813) = la date de téléchargement des notices.

![](img/2022-01-28 11_28_50-Window.jpg)


!!! attention
    Il s’agit bien de trois informations se rapportant à la même référence. Il ne faut pas créer trois références 
    distinctes comme ci-dessous :

    ![](img/2022-01-28 11_22_07-Window.jpg)

Notre premier versement *à la main* a permis de constater que l’identifiant de la notice BnF correspondant à la propriété Wikidata 
[Bibliothèque nationale de France ID (P268)](https://www.wikidata.org/wiki/Property:P268) se saisit dans Wikidata sans le 
préfixe ark:/12148/cb. Il nous faudra donc retraiter cette information avec OpenRefine avant le versement.

## Import des données dans OpenRefine

A l'import dans OpenRefine (voir pour plus de détails la [documentation de OpenRefine](https://docs.openrefine.org/manual/starting)), 
ne pas oublier de décocher la case _Use character " to enclose cells containing column 
separators_. En effet, les données à importer contiennent des guillemets (ex. _Bulletin trimestriel de l'Association 
"Al Liamm"_), qui sont la valeur par défaut de ce paramètre d'import.

![Création d'un nouveau projet dans OpenRefine](img/2022-01-31 10_48_29-Window.jpg)

!!! tip "Astuce"

    Bien penser à vérifier si le nombre de lignes du projet OpenRefine est identique à celui du fichier original.

## À propos du versement de données sur Wikidata

Pour déposer des données sur Wikidata avec OpenRefine, un compte utilisateur Wikidata (ou Wikipedia) est nécessaire.

Si vous faites des dépôts en masse dans Wikidata, il sera peut-être nécessaire d'utiliser un compte de type Bot 
(ou Robot). Les comptes de types Bot doivent être validés par Wikidata avant utilisation 
([Ouvrir une demande de compte de type Bot](https://www.wikidata.org/wiki/Wikidata:Bot_requests)).

!!! Note

    Un Bot est un script informatique, généralement écrit en Python, 
    développé pour l’édition en masse de données sur Wikipedia ou Wikidata.

!!! Note 

    le volume à partir duquel le passage à un compte de type Bot est nécessaire reste à trouver 
    dans la documentation de Wikidata.

###  Wikibase

La fonctionnalité de dépôt de données de OpenRefine [n’est pas limité à Wikidata](https://docs.openrefine.org/manual/wikibase/overview). Elle permet de 
réconcilier et de déposer des données sur toute instance du logiciel Wikibase.

[Wikibase](https://wikiba.se/) est une suite logicielle open-source maintenue par Wikimedia Deutschland. Elle a été développée 
à l'origine pour le projet Wikidata mais d'autres institutions, telles que Biblissima ou Europeana, ont 
installé des wikis avec Wikibase et mettent à disposition des fichiers de configuration, appellés *manifestes*, permettant leur édition
depuis OpenRefine. Pour en savoir plus, consultez [la documentation de OpenRefine](https://docs.openrefine.org/manual/wikibase/configuration).

!!! info

    L'éauipement Biblissima+, observatoire du patrimoine écrit du Moyen Âge et de la Renaissance du 8e au 18e siècle, 
    édite son référentiel d'autorité, [data.biblissima.fr](https://data.biblissima.fr), avec la suite logicielle Wikibase.
    [Son manifeste](https://github.com/OpenRefine/wikibase-manifests/blob/master/biblissima-data-manifest.json), 
    le fichier de configuration permettant le dépôt de données sur data.biblissima.fr avec OpenRefine est disponible sur 
    le GitHub de Openrefine.

Si vous mettez en place votre propre Wikibase, aucun service de réconciliation ne sera configuré par défaut. Pour  la réconcilation suppose, 
la mise en place d'un service de requête SPARQL et l’installation de [Wikibase reconciliation interface for OpenRefine](https://github.com/wetneb/openrefine-wikibase) en 
suivant la documentation [openrefine-wikibase.readthedocs.io](https://openrefine-wikibase.readthedocs.io/en/latest/).

### Alternatives à OpenRefine pour le dépôt de données sur Wikidata

Il existe d’autres outils pour déposer des données sur Wikidata :

- la librairie Python [PyWikiBot](https://pypi.org/project/pywikibot/), 
- le service en ligne [QuickStatements](https://quickstatements.toolforge.org), qui permet de créer et d'éditer par lot des éléments Wikidata 
- le service en ligne [Mix’n’match](https://mix-n-match.toolforge.org), qui permet d'aligner des bases de données en ligne avec des éléments Wikidata.
Ce service est utilisé par [PRELIB](https://mshb.huma-num.fr/prelib/), la base de données des acteurs de la littérature en breton pour associer les personnes et les collectifs de la 
base avec Wikidata et créer de nouveaux éléments Wikidata si nécessaire. Cependant, la création des nouveaux éléments se limite au versement d'un label
et à la déclaration de la nature de l'élément (Q5 pour les personnnes). Le versement de données complémentaires (ex. dates et lieux de naissance et de décès)
passe par un script en Python.


!!! info 
    Voir [le projet d'alignement des personnes du projet PRELIB sur Mix'n'Match](https://mix-n-match.toolforge.org/#/catalog/1679).

OpenRefine a toutefois l’avantage de combiner des fonctions de 
nettoyage, recodage, pré-traitement, alignement puis dépôt.

### Vais-je écraser des données avec mes dépôts sur Wikidata ?

Vais-je écraser des données existantes ? Vais-je être informé que j’écrase des données existantes ? Vais-je créer 
des informations en doublons ? Puis-je faire des tests ? La lecture du chapitre [Overview of Wikibase support](https://docs.openrefine.org/manual/wikibase/overview) 
de la documentation de OpenRefine prend un peu de temps mais est riche d’informations. Par exemple :

!!! cite "source : [Editing Wikidata with OpenRefine](https://docs.openrefine.org/manual/wikibase/overview#editing-wikidata-with-openrefine)"
    
    If you upload edits that are redundant (that is, all the statements you want to make have already been made), 
    nothing will happen. If you upload edits that conflict with existing information (such as a different birthdate 
    than one already in Wikidata), it will be added as a second statement. OpenRefine produces no warnings as to 
    whether your data replicates or conflicts with existing Wikidata elements.

(à suivre...)

## Alignement des titres des livres avec Wikidata

Le préalable au dépôt de données sur Wikidata est l'exécution d’un alignement (ou *réconciliation*) avec les données 
existantes de Wikidata. L'opération consiste à comparer les labels des items Wikidata avec les labels des données 
de notre fichier CSV. En l'occurrence les titres des documents et les noms des personnes.

Commençons par appliquer deux [facettes](https://docs.openrefine.org/manual/facets) :

- Une facette sur la colonne *Type de notice* et ne choisir que les monographies 
- Une facette sur la colonne *Type de document* et ne choisir que les textes imprimés. 

On tente donc d’aligner les documents à partir de leur titre stocké dans la colonne 200$a. Les notices du catalogue général ayant 
été exportées en cochant la case *"Données avec étiquettes"*, les titres sont précédés par le code de la sous-zone, `200$a`. Pour retirer cette 
chaîne de caractères, on utilise depuis le menu de la colonne `Edit cells > Transform`.

![](img/2022-02-28 16_50_31-Window.jpg)

Puis on saisit l’expression `value.replace(/^200\$a/, '')`. 

![](img/2022-02-28 16_49_58-Window.jpg)

!!! info

    Pour en savoir plus sur la signification de l'expression régulière `/^200\$a/`, 
    voir [la documentation de OpenRefine](https://docs.openrefine.org/manual/expressions#regular-expressions).

On exécute la même opération de remplacement sur la colonne `010$a` (= l’ISBN-13) avec cette fois-ci 
l’expression `value.replace(/^010\$a/, '')`. L'ISBN-13 sera le deuxième critère qui sera soumis au service de réconciliation
de Wikidata. 

!!! tip 
    Retrospectivement, l'ISBN-13 seul aurait suffit à l'alignement pour la période choisie (les documents publiés depuis 2012).
    En effet, l'ISBN-13 identifie de manière unique un document et doit obligatoirement être mentionné sur tout livre publié
    (source : [Quelles sont les mentions obligatoires sur un livre papier ou numérique ?](https://entreprendre.service-public.fr/vosdroits/F22672)).

On lance ensuite la réconciliation depuis le menu de la colonne `200$` : `Reconcile > Start reconciling`.

![](img/2022-02-28 16_12_26-Window.jpg)

![](img/2022-02-28 16_13_34-Window.jpg)

On choisi `Reconcile against type: Q3331189`, en décochant `“Auto-match candidates with high confidence”` 
et en associant la colonne `010$a` (= l’ISBN-13) avec https://www.wikidata.org/wiki/Property:P212.

![](img/2022-02-28 15_14_55-Window.jpg)

La réconciliation suggère l’alignement du *Dictionnaire étymologique du breton* (ISBN 978-2-915915-45-7) publié en 2021
avec *[Dictionnaire étymologique du breton (Q50915490)](https://www.wikidata.org/wiki/Q50915490)* mais il s’agit de 
l’édition de 2003. Il y a aussi des suggestions pour *Bistro* mais elles ne correspondent pas à l’ouvrage en breton. 
On peut marquer ces deux livres comme à créer par la coche `"Create new item"`.

![](img/2022-01-28 16_16_45-ExportProBreton2021 - OpenRefine.jpg)

Des livres ont été créés sur Wikidata lors de la préparation de cet atelier, ils correspondent à des scores 
de 100, à afficher avec la facette `"Best candidate’s score"` appliquée sur la colonne `200$a`. Dans la facette, 
cocher la case `"Numeric"`puis déplacer le curseur de manière à réduire la fenêtre d'affichage.

![](img/2022-04-11 11_28_15-Window.png)

!!! tip 
    Si vous avez perdu l'affichage de la facette `"Best candidate’s score"`, vous pouvez la faire réapparaitre 
    dans le menu de la colonne sur laquelle une réconciliation a été appliquée : `Reconcile > Facets > Best candidate's score`.

Les suggestions d'alignement des livres déjà présents sur Wikidata peuvent être validés par la coche en regard de la suggestion
(la mention *Match this item to this cell* apparaît au survol de la coche).

![](img/2022-02-28 15_24_35-Window.jpg)

On choisit ensuite tous les livres pour lesquels il n’y a pas encore d’équivalent sur Wikidata avec la facette 
`Best candidate’s score`, case à cocher `Blank`.

![](img/2022-04-11 11_28_15-Window.png)

Et on les marque d’un coup d’un seul comme à créer depuis le menu de la colonne 
`200$a > Reconcile > Actions > Create a new item for each cell` :

![](img/2022-01-27 17_29_10-Window.jpg)

!!! Info 

    L'opération `Create a new item for each cell` ne crée pas immediatement des items sur Wikidata mais marque 
    les cellules de la colonne du projet OpenRefine en vue d’une future opération de création.
    
    Notez que ce marquage ne s’opère que sur les lignes sélectionnées par les facettes ou des filtres.

    Il est possible de rattacher plusieurs lignes au même item Wikidata à créer. Voir la chapitre 
    [Marking multiple cells as identical items](https://docs.openrefine.org/manual/wikibase/new-entities#marking-multiple-cells-as-identical-items)
    dans la documentation de OpenRefine.

## Alignement des auteurs des livres avec Wikidata

En UNIMARC, le bloc `7xx` permet de saisir les responsabilités, c’est à dire toute personne ou collectif ayant contribué 
à la production et la création du document (auteur, traducteur, préfacier, illustrateur, éditeur scientifique, etc.).

Notre export des notices du catalogue général ne concerne que le code *700 Nom de personne - Responsabilité principale*. 
On applique les mêmes méthodes que précédemment pour supprimer la première étiquette de chaque colonne.

Par exemple, pour le code fonction `700$4` l’expression GREL = `value.replace(/^700\$4/, '')`

Si un document avait eu plusieurs personnes en responsabilité principale, on aurait dû les répartir sur plusieurs 
lignes avant l’alignement mais cela n’est pas le cas.

Pour la colonne `700$o` qui correspond à l’ISNI (c.f. [la documentation](https://www.ifla.org/wp-content/uploads/2019/05/assets/uca/unimarc_updates/BIBLIOGRAPHIC/u-b_700_update.pdf)), 
on retire également la chaine “ISNI” : `value.replace(/^700\$oISNI/, '')`.

Les labels des personnes sur Wikidata sont constitués du prénom et du nom attachés. On crée une colonne `700$b700$a` 
qui concatène `700$b` et `700$a` à partir de la colonne `700$b` :  `value + ' ' + cells["700$a"].value`

![](img/2022-03-01 09_01_35-ExportProBreton2021DonneesAvecEtiquette - OpenRefine.jpg)

Nous souhaitons pour l'instant ne verser que les auteurs, c'est à dire les individus ayant pour code fonction `700$4` = 070
(c.f. [Annexe C – Codes de fonction – Liste numérique](https://www.transition-bibliographique.fr/wp-content/uploads/2021/06/BAnnC-2021.pdf)). 
On applique une facette textuelle sur la colonne `700$4` et on sélectionne la valeur `070`.

![](img/2022-04-14 09_52_24-ExportProBreton2021DonneesAvecEtiquette - OpenRefine.png)

On réconcilie le contenu de la colonne `700$b700$a` avec les entités de type Wikidata human Q5 en associant la colonne 
`700$o` avec la propriété Wikidata ISNI.

![](img/2022-03-01 09_06_03-ExportProBreton2021DonneesAvecEtiquette - OpenRefine.jpg)

## Préparation de l'export sur Wikidata

La colonne `“Identifiant ARK”` contient l'identifiant pérenne de chaque notice. Par exemple la notice du livre
pour enfant *An div ran a oa bras o beg* (*Les deux grenouilles à grande bouche*) a pour identifiant `ark:/12148/cb46811801p`. 

Mais comme vu en introduction, l’identifiant de la notice BnF se saisit avec la propriété Wikidata 
[Bibliothèque nationale de France ID (P268)](https://www.wikidata.org/wiki/Property:P268) sans le 
préfixe ark:/12148/cb. Nous avons également prévu de signaler l'URL de la notice dans les sources des déclarations 
(ou *statements*). Par exemple l'URL pérenne de la notice de *An div ran a oa bras o beg* est `http://ark.bnf.fr/ark:/12148/cb46811801p`.

### Création de deux nouvelles colonnes

À partir de la colonne `“Identifiant ARK”` on choisit la fonction `Edit column > Add column based on this column`.

![](img/2022-02-28 17_22_43-Window.jpg)

On nomme la nouvelle colonne `URLARKNotice` et on saisi l'expression : "http://ark.bnf.fr/" + value.

![](img/2022-01-28 11_21_20-Window.jpg)

À partir de la colonne `“Identifiant ARK”` on choisit de nouveau la fonction `Edit column > Add column based on this column`.

On nomme cette nouvelle colonne `P269` et on saisi l'expression : `value.replace("ark:/12148/cb","")`.

![](img/2022-01-31 14_47_17-Window.jpg)

### Filtrage des documents publiés en 2021

La date de publication dans la colonne `214$d`, nécessite aussi quelques ajustements. Ce champ UNIMARC ne contient pas 
que des années de publications. Par exemple, c’est l’année de dépôt légal qui est parfois indiquée, précédée par la 
mention *DL* suivie d’un espace. Par simplification, on ne choisit dans la facette que les documents dont la valeur = 2021, sans lettres.

# Export des données vers Wikidata

## Schéma d'export

Pour préparer l’export, il faut créer un schéma (*Export > Wikibase schema* en haut à droite ou *Extensions Wikidata > 
Edit Wikibase schema* en haut à droite dans la barre bleue). 

Le schéma peut être sauvegardé et exporté. Il ne s’applique qu’aux lignes sélectionnées par les facettes et les 
filtres et aux cellules avec le marquage *create new item* (voir les chapitres précédents).

![](img/2022-03-01 09_20_29-.jpg)

## Export d'un premier livre

Avec la facette textuelle de la colonne Identifiant ARK, on ne sélectionne qu’un seul des livres. 
Dans le menu *Extensions > Wikidata > Manage Wikibase account* pour se logger avec un compte Wikidata (ou Wikipedia) 
puis *Extensions > Wikidata > Upload edits to Wikibase*. Une fois l’upload terminé, le titre du livre (colonne `200$a`) 
est affiché aligné avec l’entité Wikidata qui vient d’être créée.


![](img/2022-01-31 16_39_37-Window.jpg)

Si l'essai est concluant, on peut supprimer la facette qui limite l'affichage à un seul livre et conserver la facette 
textuelle sur la colonne `700$4` avec la valeur `070` sélectionnée (Limitation aux seuls auteurs) pour exporter les 
documents restants.

# Bibliographie

- [Creating new Wikidata items with OpenRefine and Quickstatements](https://addshore.com/2020/07/creating-new-wikidata-items-with-openrefine-and-quickstatements/) (par Adam Shorland, ingénieur informatique à Wikimedia Deutschland)
- [Editing Wikidata with OpenRefine](https://docs.openrefine.org/manual/wikibase/overview) (documentation de OpenRefine)
