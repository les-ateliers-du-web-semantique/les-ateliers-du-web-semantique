# Exploration de data.bnf.fr

Notes des ateliers du mardi 5 avril 2022 et du 3 mai 2022 (10h-11h) en visio.

Auteur : Jean-Baptiste Pressac (Centre de recherche bretonne et celtique - CNRS/UBO)

Version du vendredi 3 juin 2022

## Objectifs de l'atelier

La Bibliothèque nationale de France (BnF) publie sur le portail [data.bnf.fr](https://data.bnf.fr/) des informations 
sur des œuvres (par ex. le roman *[The shining](https://data.bnf.fr/fr/16986879/stephen_king_the_shining/)*
de Stephen King publié pour la première fois en 1977 aux Etats-Unis), sujets, personnes, organisations, thèmes, lieux, dates… selon les standards du Web sémantique (URI, triplets RDF, ontologies).

Les ressources RDF correspondantes peuvent être interrogées depuis un [SPARQL endpoint](https://data.bnf.fr/sparql/).

L'objectif de l'atelier est d'interroger le SPARQL endpoint de data.bnf.fr pour obtenir la liste des éditions d'œuvres 
traduites en breton en partant de l'exemple du roman de Stephen King *[The shining](https://data.bnf.fr/fr/16986879/stephen_king_the_shining/)* 
qui a fait l'objet d'une traduction en breton publiée en 2014 par la maison d'édition *Mouladurioù Hor Yezh*.

## Le modèle de données de data.bnf.fr

Selon le site [BnF API et jeux de données](source : https://api.bnf.fr/fr/sparql-endpoint-de-databnffr ) :

*data.bnf.fr rassemble les données issues des différentes bases et catalogues de la BnF pour y donner un accès fédéré 
par auteurs, œuvres, thèmes, lieux et dates. Les données de data.bnf.fr sont enrichies par des alignements avec d'autres 
données publiées sur le Web, comme Wikidata ou DBpedia. Elles sont exprimées selon les standards du Web sémantique et 
sont récupérables au format RDF (XML, NT, N3) et JSON ou JSON-LD.*

Le modèle de données est décrit en détail sur la page [Web sémantique et modèle de données](https://data.bnf.fr/fr/semanticweb)
de data.bnf.fr. Mais pour faire court, il repose sur un modèle dérivé du FRBR (*Functional Requirements for Bibliographic Record*), 
un modèle conceptuel de bibliographie qui incite les bibliothèques à ne plus seulement cataloguer des exemplaires de 
documents mais à exprimer les œuvres de l'esprit qu'elles contiennent 
et les relations entre ces œuvres. L'objectif étant notamment d'améliorer la recherche de documents. Pour en savoir plus 
sur le FRBR et ses objectifs, voir 
*[FRBR, qu’est-ce que c’est ? Un modèle conceptuel pour l’univers bibliographique](https://www.loc.gov/catdir/cpso/FRBRFrench.pdf)* de Barbara Tillett.

Le modèle FRBR peut être résumé par le schéma suivant (source : [Fonctionnalités requises des notices bibliographiques : rapport final / Groupe de
travail IFLA sur les Fonctionnalités requ ises des notices bibliographiques. — 2e édition française / établie par la Bibliothèque nationale de France. — Paris : Bibliothèque nationale de France, 2012](https://multimedia-ext.bnf.fr/pdf/frbr_rapport_final.pdf)) :

``` mermaid
graph TB
  Oeuvre -->|"trouve sa réalisation dans"|Expression;
  Expression -->|"se concrétise dans"|Manifestation;
  Manifestation -->|"est représentée par"|Item;
```

Le roman de Stephen King *[The shining](https://data.bnf.fr/fr/16986879/stephen_king_the_shining/)* 
que l'on trouve sur data.bnf.fr se situe au niveau *Work*, c'est à dire sous la forme d'une œuvre de l'esprit. 
On ne parle ni du manuscrit original, ni des brouillons, ni même des phrases qui constituent 
le roman, on parle uniquement du concept de ce roman imaginé par Stephen King (le charmant hôtel
*Overlook*, les montagnes du Colorado l'hiver, Danny Torrance et son don de vision, Jack Torrance et sa hache...). 

La forme que prend l'œuvre de l'esprit correspond se situe au niveau *Expression*. Dans le
cas de *The shining*, l'œuvre est exprimée sous forme de mots. S'il s'agissait de lire le roman à haute voix et 
de l'enregistrer, l'expression serait de nature sonore. La notion de langue est rattachée à l'*expression*. 

L'*édition*, c'est à dire la publication de l'expression correspond à la *Manifestation* avec les notions d'éditeur,
de titre du livre publié, de date de publication... 

L'exemplaire du livre que vous pouvez emprunter sur le site de Tolbic de la BnF (cote 8-Y2-97606) correspondant à l'*Item*.

``` mermaid
graph TB
  O("Oeuvre « The shining », roman écrit par Stephen King") --> |"trouve sa réalisation dans"| E("Expression de l'édition Shining (2014), Stephen King, [Lannion] : Moul. Hor yezh , impr. 2014") ;
  E --> |"se concrétise dans"| M("Manifestation : l'édition Shining (2014), Stephen King, [Lannion] : Moul. Hor yezh , impr. 2014") ;
  M --> |"est représentée par"| I("Item : l'exemplaire de Shining en breton conservé à la BnF sous la cote 2014-166848, empruntable à Tolbiac") ;
```

On peut aussi prendre le problème en partant de l'exemplaire, ce qui permet de préciser que l'expression est liée à la manifestation 
(i.e. l'édition).

``` mermaid
graph BT
  E("Expression de l'édition Shining (2014), Stephen King, [Lannion] : Moul. Hor yezh , impr. 2014")  --> |"réalise"| O("Oeuvre « The shining », roman écrit par Stephen King") ;
  M("Manifestation : l'édition Shining (2014), Stephen King, [Lannion] : Moul. Hor yezh , impr. 2014") --> |"matérialise"| E ;
  I("Item : l'exemplaire de Shining en breton conservé à la BnF sous la cote 2014-166848, empruntable à Tolbiac") --> |"exemplifie"| M ;
```

## Les questions que l'on peut se poser

Le modèle de données ne nous indique pas comment les données relatives à l'œuvre *The shining* et à ses différentes 
éditions sont exprimées en RDF.

- Quelles sont les URIs correspondant à la ressource RDF *The shining*, à ses expressions et à ses manifestations ?
- Quelles propriétés RDF lient entre elles ces ressources ? Autrement dit, quelles ontologies ont été utilisées ?
- Comment est exprimée la notion de traduction sur data.bnf.fr ?

## Exploration des données RDF de *The shining*

Plutôt que de se servir de [la documentation de data.bnf.fr](https://data.bnf.fr/fr/semanticweb), nous avons préféré utiliser 
la fonction SPARQL DESCRIBE pour explorer pas à pas le graphe de *The shining* depuis le [SPARQL endpoint](https://data.bnf.fr/sparql/).

Si l'on part de l'URL de la page de *[The shining](https://data.bnf.fr/fr/16986879/stephen_king_the_shining/)*, la requête
ne renvoie aucun résultat.

`DESCRIBE <https://data.bnf.fr/fr/16986879/stephen_king_the_shining/>`

Pas mieux avec le permalien indiqué en bas de page.

`DESCRIBE <https://data.bnf.fr/ark:/12148/cb169868794>`

Par contre, en retirant le s de https du permalien, on obtient des triplets.

`DESCRIBE <http://data.bnf.fr/ark:/12148/cb169868794>`

Le premier triplet à regarder est celui qui correspond à la classe de la ressource RDF.

`<http://data.bnf.fr/ark:/12148/cb169868794> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.w3.org/2004/02/skos/core#Concept>`

Triplet qui aurait pu être écrit de la manière suivante en Turtle :

``` turtle
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .

<http://data.bnf.fr/ark:/12148/cb169868794> a skos:Concept .
```

La ressource `<https://data.bnf.fr/ark:/12148/cb169868794>` ne correspond donc pas à l'œuvre *The shining*. Cependant, 
elle a pour `foaf:focus` une autre ressource, `<https://data.bnf.fr/ark:/12148/cb169868794#about>` que l'on explore 
à son tour avec DESCRIBE.

`DESCRIBE <http://data.bnf.fr/ark:/12148/cb169868794#about>`
	
<http://data.bnf.fr/ark:/12148/cb169868794#about> est une instance des classes <http://rdaregistry.info/Elements/c/C10001>
(label : "work") et <http://rdvocab.info/uri/schema/FRBRentitiesRDA/Work>. 

Visiblement, on a mis la main sur la ressource RDF qui représente l'œuvre *The shining*. 

Notons au passage que la langue de *The shining* est exprimée avec la propriété `dcterms:language`.

L'œuvre est liée à ses expressions par la propriété <http://rdvocab.info/RDARelationshipsWEMI/expressionOfWork> 
et à ses manifestations (ses éditions) par la propriété <http://rdvocab.info/RDARelationshipsWEMI/workManifested>.

``` turtle
# un extrait des triplets dont la ressource The shining (ark:/12148/cb169868794) est sujet ou objet :
@prefix arkbnf: <http://data.bnf.fr/ark:/12148/> .
@prefix rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/> .

arkbnf:cb169868794#about rdarelationships:expressionOfWork arkbnf:cb43868006h#Expression .
arkbnf:cb34642708z#about rdarelationships:workManifested arkbnf:cb169868794#about .
```

On résume ces premiers triplets dans le graphe suivant.

``` sparql
PREFIX arkbnf: <http://data.bnf.fr/ark:/12148/>
PREFIX frbr-rda: <http://rdvocab.info/uri/schema/FRBRentitiesRDA/>
PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX iso639-2: <http://id.loc.gov/vocabulary/iso639-2/>
PREFIX marcrel: <http://id.loc.gov/vocabulary/relators/>
```
``` mermaid
graph TB
  O("arkbnf:cb169868794#about") --> |rdarelationships:expressionOfWork| E1("arkbnf:cb43868006h#Expression");
  O --> |rdf:type| Work_Class("frbr-rda:Work");
  O --> |rdfs:label| O_Label(The shining);  
  O --> |dcterms:date| O_Date(1977);
  O --> |dcterms:language| O_Language(iso639-2:eng);
  E1 --> |rdf:type| Expression_Class("frbr-rda:Expression"); 
  M1("arkbnf:cb43868006h#about") --> |rdarelationships:expressionManifested| E1;
  M1 --> |rdarelationships:workManifested| O;
  M1 --> |rdf:type| Manifestation_Class("frbr-rda:Manifestation");  
```


!!! info

    On peut visualiser en ligne le résultat de la requête `DESCRIBE` sur l'œuvre *The shining* avec [:isSemantic](https://issemantic.net/rdf-visualizer).
    Depuis la page du SPARQL endpoint, on clique sur l'onglet *Response*.

    ![](img/2022-04-29 14_40_59-Éditeur SPARQL de data.bnf.fr.png)

    On copie avec un clic droit sur *Permalien vers les résultats* pour copier le lien vers la requête.  

    ![](img/2022-04-29 14_41_15-Éditeur SPARQL de data.bnf.fr.png)

    Lien que l'on colle dans *Enter website address* de [https://issemantic.net/rdf-visualizer](https://issemantic.net/rdf-visualizer) :

    ![](img/2022-04-29 14_41_44-Online RDF graph visualizer_ Turtle, JSON-LD, TriG, RDF-star.png)

    :isSemantic affiche le graphe suivant :

    ![](img/2022-04-29 14_41_57-Online RDF graph visualizer_ Turtle, JSON-LD, TriG, RDF-star.png)

### Triplets des expressions de *The shining*

Si on explore avec DESCRIBE une des expressions prises au hasard, `<http://data.bnf.fr/ark:/12148/cb43868006h#Expression>`
on constate qu'elle est liée par la propriété 
`<http://purl.org/dc/terms/language>` avec une ou plusieurs langues, exprimées avec des ressources publiées 
par la bibliothèque du Congrès (*Library of Congress*).

`<http://data.bnf.fr/ark:/12148/cb43868006h#Expression> <http://purl.org/dc/terms/language> <http://id.loc.gov/vocabulary/iso639-2/bre>`

Le hasard fait bien les choses, il s'agit d'une expression en breton. Elle est le sujet de deux triplets qui retiennent notre attention.

``` turtle
@prefix arkbnf: <http://data.bnf.fr/ark:/12148/> .
@prefix rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/> .
@prefix marcrel: <http://id.loc.gov/vocabulary/relators/> .

arkbnf:cb43868006h#Expression marcrel:aut arkbnf:cb11909772t#about .
arkbnf:cb43868006h#Expression marcrel:trl arkbnf:cb16623239r#about .
```

Il s'agit de la déclaration de l'auteur de l'expression, Stephen King (représenté par la ressource `<http://data.bnf.fr/ark:/12148/cb11909772t#about>`) 
 et du traducteur (*translator* en anglais), Olivier Biguet (représenté par la ressource `<http://data.bnf.fr/ark:/12148/cb16623239r#about>`).

Ces informations sont également exprimées avec les propriétés `<http://data.bnf.fr/vocabulary/roles/r680>` et 
`<http://data.bnf.fr/vocabulary/roles/r70>` mais la signification des codes *r680* et *r70* est moins explicite au premier coup 
d'œil que celles des propriétés *Author* et *Translator* du vocabulaire [MARC Code List for Relators Scheme](https://id.loc.gov/vocabulary/relators.html) 
de la bibliothèque du Congrès.

``` sparql
PREFIX arkbnf: <http://data.bnf.fr/ark:/12148/>
PREFIX frbr-rda: <http://rdvocab.info/uri/schema/FRBRentitiesRDA/>
PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX iso639-2: <http://id.loc.gov/vocabulary/iso639-2/>
PREFIX marcrel: <http://id.loc.gov/vocabulary/relators/>
```
``` mermaid
graph TB
  O("arkbnf:cb169868794#about") --> |rdarelationships:expressionOfWork| E1("arkbnf:cb43868006h#Expression");
  O --> |rdf:type| Work_Class("frbr-rda:Work");
  O --> |rdfs:label| O_Label(The shining);  
  O --> |dcterms:date| O_Date(1977);
  O --> |dcterms:language| O_Language(iso639-2:eng);  
  E1 --> |rdf:type| Expression_Class("frbr-rda:Expression");
  E1 --> |dcterms:language| FRE("iso639-2:bre");
  E1 --> |marcrel:aut| E1_Aut("arkbnf:cb11909772t#about");
  E1_Aut --> |foaf:name| E1_Aut_Name(Stephen King)
  E1 --> |marcrel:trl| E1_Trl("arkbnf:cb16623239r#about");
  E1_Trl --> |foaf:name| E1_Trl_Name(Olivier Biguet)   
```

### Triplets de l'édition en breton de *The shining*

La manifestation, soit l'édition correspondante de cette version en breton de *The shining* est liée à l'expression par 
la propriété `<http://rdvocab.info/RDARelationshipsWEMI/expressionManifested>`.

`<http://data.bnf.fr/ark:/12148/cb43868006h#about> <http://rdvocab.info/RDARelationshipsWEMI/expressionManifested> <http://data.bnf.fr/ark:/12148/cb43868006h#Expression>`

DESCRIBE appliqué à la manifestation `<http://data.bnf.fr/ark:/12148/cb43868006h#about>` renvoie des triplets indiquant que 

- la ressource
appartient bien aux classes `<http://rdvocab.info/uri/schema/FRBRentitiesRDA/Manifestation>` et 	
<http://rdaregistry.info/Elements/c/C10007> (dont le label est *manifestation*)
- Le titre de l'édition en breton est *Shining* comme indiqué par la propriété `dc:title`
- l'ISBN 13 est indiquée par la propriété `bibo:isbn13`
- la version bretonnne a été imprimée en 2014 à Lannion par *Hor Yezh*. Cette information est exprimée par plusieurs 
triplets. Nous choisirons pour nos prochaines requêtes les triplets dont le prédicat est la propriété `<http://purl.org/dc/terms/publisher>`
qui résume ces trois informations. Notez au passage que *moul.* dans *[Lannion] : Moul. Hor yezh , impr. 2014*
est l'abréviation du breton *mouladenn* (impression, édition).

### Quid des items ?

data.bnf.fr n'exprime pas en RDF d'informations sur les items. Cependant, la propriété rdfs:seeAlso 
permet d'obtenir l'URL de la notice équivalente sur le catalogue général de la BnF qui donne des informations 
sur la localisation des exemplaires conservés à la BnF. 

```
<http://data.bnf.fr/ark:/12148/cb43868006h#about> <http://www.w3.org/2000/01/rdf-schema#seeAlso> <https://catalogue.bnf.fr/ark:/12148/cb43868006h>
```

### Le graphe de l'édition en breton de *The shining*

En résumé, 

- URI de l'oeuvre *The shining* : [<http://data.bnf.fr/ark:/12148/cb169868794#about>](http://data.bnf.fr/ark:/12148/cb169868794#about)
- URI de l'expression de l'édition en breton de *The shining* publiée en 2014 par *Mouladurioù Hor Yezh* : [<http://data.bnf.fr/ark:/12148/cb43868006h#Expression>](http://data.bnf.fr/ark:/12148/cb43868006h#Expression)
- URI de la manifestation correspondant à édition en breton de *The shining* publiée en 2014 par *Mouladurioù Hor Yezh* : [<http://data.bnf.fr/ark:/12148/cb43868006h#about>](http://data.bnf.fr/ark:/12148/cb43868006h#about)
- URI de Stephen King : [<http://data.bnf.fr/ark:/12148/cb11909772t#about>](<http://data.bnf.fr/ark:/12148/cb11909772t#about>)
- URI de Olivier Biguet (traducteur en breton) : [http://data.bnf.fr/fr/16623239/olivier_biguet/#about]() 

On obtient le graphe suivant liant l'œuvre *The shining* à son édition en breton :

``` sparql
PREFIX arkbnf: <http://data.bnf.fr/ark:/12148/>
PREFIX frbr-rda: <http://rdvocab.info/uri/schema/FRBRentitiesRDA/>
PREFIX rdarelationships: <http://rdvocab.info/RDARelationshipsWEMI/>
PREFIX dcterms: <http://purl.org/dc/terms/>
PREFIX iso639-2: <http://id.loc.gov/vocabulary/iso639-2/>
PREFIX marcrel: <http://id.loc.gov/vocabulary/relators/>
```
``` mermaid
graph TB
  O("arkbnf:cb169868794#about") --> |rdarelationships:expressionOfWork| E1("arkbnf:cb43868006h#Expression");
  O --> |rdf:type| Work_Class("frbr-rda:Work");
  O --> |rdfs:label| O_Label(The shining);  
  O --> |dcterms:date| O_Date(1977);
  O --> |dcterms:language| O_Language(iso639-2:eng);  
  E1 --> |rdf:type| Expression_Class("frbr-rda:Expression");
  E1 --> |dcterms:language| FRE("iso639-2:bre");
  E1 --> |marcrel:aut| E1_Aut("arkbnf:cb11909772t#about");
  E1_Aut --> |foaf:name| E1_Aut_Name(Stephen King)
  E1 --> |marcrel:trl| E1_Trl("arkbnf:cb16623239r#about");
  E1_Trl --> |foaf:name| E1_Trl_Name(Olivier Biguet)  
  M1("arkbnf:cb43868006h#about") --> |rdarelationships:expressionManifested| E1;
  M1 --> |rdarelationships:workManifested| O;
  M1 --> |rdf:type| Manifestation_Class("frbr-rda:Manifestation");
  M1 --> |dcterms:title| M1_Title("Shining");
  M1 --> |dcterms:publisher| M1_Publisher("[Lannion] : Moul. Hor yezh , impr. 2014");
  M1 --> |bibo:isbn13| M1_ISBN13("978-2-86863-171-8");    
```

!!! note

    Pour afficher le graphe de l'oeuvre *The shining* et de ses expressions avec :isSemantic, on peut utiliser la requête
    SPARQL:

    ``` sparql
    DESCRIBE <http://data.bnf.fr/ark:/12148/cb169868794#about> ?expressions 
    WHERE {
        <http://data.bnf.fr/ark:/12148/cb169868794#about> <http://rdvocab.info/RDARelationshipsWEMI/expressionOfWork> ?expressions .
    }

    ```

## Requête SPARQL pour afficher les éditions en breton de *The shining*

On obtient au final la [requête SPARQL suivante](https://data.bnf.fr/sparql/#query=SELECT%20(SAMPLE(%3Ftitle)%20AS%20%3Ftitre_edition)%20(SAMPLE(%3Fpublisher)%20AS%20%3Fediteur)%20(GROUP_CONCAT(%3Fnom_traducteur%3B%20separator%20%3D%22%2C%22)%20AS%20%3Fnoms_traducteurs)%0A(GROUP_CONCAT(%3Fnom_auteur%3B%20separator%20%3D%22%2C%22)%20AS%20%3Fnoms_auteurs)%0AWHERE%20%7B%0A%20%3Fmanifestation%20%3Chttp%3A%2F%2Frdvocab.info%2FRDARelationshipsWEMI%2FworkManifested%3E%20%3Chttp%3A%2F%2Fdata.bnf.fr%2Fark%3A%2F12148%2Fcb169868794%23about%3E%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Chttp%3A%2F%2Frdvocab.info%2FRDARelationshipsWEMI%2FexpressionManifested%3E%20%3Fexpression%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2Ftitle%3E%20%3Ftitle%20%3B%0A%20%20%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2Fpublisher%3E%20%3Fpublisher%20.%0A%20%20%3Fexpression%20%3Chttp%3A%2F%2Fpurl.org%2Fdc%2Fterms%2Flanguage%3E%20%3Chttp%3A%2F%2Fid.loc.gov%2Fvocabulary%2Fiso639-2%2Fbre%3E%20%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Chttp%3A%2F%2Fid.loc.gov%2Fvocabulary%2Frelators%2Ftrl%3E%20%3Ftraducteur%20%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Chttp%3A%2F%2Fid.loc.gov%2Fvocabulary%2Frelators%2Faut%3E%20%3Fauteur.%0A%20%20%3Ftraducteur%20%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fname%3E%20%3Fnom_traducteur%20.%0A%20%20%3Fauteur%20%3Chttp%3A%2F%2Fxmlns.com%2Ffoaf%2F0.1%2Fname%3E%20%3Fnom_auteur%20.%0A%7D%20GROUP%20BY%20%3Fmanifestation%0A&endpoint=https%3A%2F%2Fdata.bnf.fr%2Fsparql&requestMethod=POST&tabTitle=Query&headers=%7B%7D&contentTypeConstruct=text%2Fturtle%2C*%2F*%3Bq%3D0.9&contentTypeSelect=application%2Fsparql-results%2Bjson%2C*%2F*%3Bq%3D0.9&outputFormat=table) pour afficher les traductions en breton de *The shining* :

``` sparql
SELECT (SAMPLE(?title) AS ?titre_edition) (SAMPLE(?publisher) AS ?editeur) (GROUP_CONCAT(?nom_traducteur; separator =",") AS ?noms_traducteurs)
(GROUP_CONCAT(?nom_auteur; separator =",") AS ?noms_auteurs)
WHERE {
# <http://data.bnf.fr/ark:/12148/cb169868794#about> = l'oeuvre The shining
 ?manifestation <http://rdvocab.info/RDARelationshipsWEMI/workManifested> <http://data.bnf.fr/ark:/12148/cb169868794#about> ;
                <http://rdvocab.info/RDARelationshipsWEMI/expressionManifested> ?expression ;
                <http://purl.org/dc/terms/title> ?title ;
  <http://purl.org/dc/terms/publisher> ?publisher .
  ?expression <http://purl.org/dc/terms/language> <http://id.loc.gov/vocabulary/iso639-2/bre>  ;
              <http://id.loc.gov/vocabulary/relators/trl> ?traducteur ;
              <http://id.loc.gov/vocabulary/relators/aut> ?auteur.
  ?traducteur <http://xmlns.com/foaf/0.1/name> ?nom_traducteur .
  ?auteur <http://xmlns.com/foaf/0.1/name> ?nom_auteur .
} GROUP BY ?manifestation
```

Saurez-vous écrire une requête générique pour afficher toutes les éditions de traductions d'oeuvres en breton ? La suite au prochain atelier, mardi 3 mai 2022 de 10h à 11h en visio.
